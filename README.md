# Drupal scaffold files 

## Drupal 8

```sh
curl -fsSL "https://bitbucket.org/cetro/drupal-scaffold/raw/master/settings.php" -o ./docroot/sites/default/settings.php

curl -fsSL "https://bitbucket.org/cetro/drupal-scaffold/raw/master/settings.local.php" -o ./docroot/sites/default/settings.local.php

curl -fsSL "https://bitbucket.org/cetro/drupal-scaffold/raw/master/.htaccess" -o ./docroot/.htaccess
```

## Drupal 7

```sh
curl -fsSL "https://bitbucket.org/cetro/drupal-scaffold/raw/drupal7/settings.php" -o ./sites/default/settings.php

curl -fsSL "https://bitbucket.org/cetro/drupal-scaffold/raw/drupal7/settings.local.php" -o ./sites/default/settings.local.php

curl -fsSL "https://bitbucket.org/cetro/drupal-scaffold/raw/drupal7/.htaccess" -o ./.htaccess
```

## General

### Redirect all files to another domain in .htaccess
`RewriteRule ^(sites/default/files/.*) https://museerne.dk/$1 [L]`